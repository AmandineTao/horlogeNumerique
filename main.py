from datetime import datetime
import pytz, time

if __name__ == '__main__':
    '''
    Horloge Numerique
    '''

    while True:
        tz1 = pytz.timezone("Africa/Douala")
        tz2 = pytz.timezone("Europe/Paris")
        tz3 = pytz.timezone("America/New_York")

        # for each time zone, get current time in specific format, print it and erase screen with flush
        lt1 = datetime.now(tz1).strftime("%H:%M:%S ")
        print("Heure Douala: ", lt1, end="", flush=True)

        lt2 = datetime.now(tz2).strftime("%H:%M:%S ")
        print("<--> Heure Paris: ", lt2, end="", flush=True)

        lt3 = datetime.now(tz3).strftime("%H:%M:%S ")
        print("<--> Heure New_York: ", lt3, end="", flush=True)

        # stop program for 1s to get both printing at the same time
        time.sleep(1)

        # erase the screen and mettre le pointeur au debut de la ligne
        print("\r", end="", flush=True)
